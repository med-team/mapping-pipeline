from abc import ABCMeta, abstractmethod
import argparse
import os
import gzip
import custom_logger as log
import re
import sys
if sys.version_info < (3, 2):
    import subprocess32 as sp
else:
    import subprocess as sp


class Mapper:
    """ 
    This is the abstract class to use when implementing a new mapper. 
    Try to minimize the output in the console, e.g. redirect the output of a call to dev/null or save it somewhere.
    Make sure if you may want to use os.path.expanduser(PATH) to extend e.g. '~' to the home directory.
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, query, reference, output, options=None, logger=None):
        """ 
        Input:
            query: Path to the query file. 
                Type: str for single-end reads, list of two str for paired-end reads.
            reference: Path to the reference file. 
                Type: str
            output: Path to the output file without the file extension (e.g. '.sam').
                Type: str
            options: Contains all additional options.
                Type: list of str, organized like sys.argv
            logger: Optional log.
                Type: logging.logger object

        Accessed from outside:
            statistics: String with mapper statistics.
                Type: str
            version: Version of the used mapper.
                Type: str

        Other:
            In self.options may be given: A path where to save the index. Default: '~/tmp/'
            e.g.:
                index_location: Expected path to the index, i.e. where it should be saved. Default: '~/tmp/'
                    Type: str
                index: Real path to the index.
                    Type: str

            and
            verbosity: May be given in self.options.
                        1: Report steps.
                Type: int

        You may add more initialization, e.g. detection of input file types (e.g. .fa or .fastq in Bowtie2).

        self.report() at the end.
        """

        self.query = query
        self.reference = os.path.expanduser(reference)
        self.query_paired = len(query) == 2 #isinstance(query, list)
        self.output = os.path.expanduser(output) # For example, without file extension
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
        else:
            self.query = os.path.expanduser(self.query[0])
        self.statistics = None
        self.version = None
        self.index = None
        self.logger = logger
        self.verbosity = 1
        # parse options
        self.report()
    
    @abstractmethod
    def map(self):
        """ Create index if non exists (self.index). Call mapper to map self.query to self.reference using additional options. 
            Automatically detects single- or paired-end reads. 
            Return the absolute path of the location of the generated SAM-file.
            Return None if an error occurs.
            If the console output contains statistics (e.g. in Bowtie2), these should be saved in self.statistics."""

    @abstractmethod
    def get_version(self):
        """ Return the version. """
        return self.version

    @abstractmethod
    def write_unmapped(self, un):
        """ Write unaligned reads to un. """

    @abstractmethod
    def get_statistics(self):
        """ Return the statistics. """
        return self.statistics 
    
    @abstractmethod
    def cleanup(self):
        """ Remove the index. You may want to use 'find' instead of 'rm'."""

    @abstractmethod
    def report(self):
        """ Write all meaningful variables (at least the one defined in __init__) and their respective values to the logger. """

class Bowtie2(Mapper):

    def __init__(self, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bowtie2=None):
        self.query = query
        self.reference = os.path.expanduser(reference)
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        self.reference_fasta = reference.endswith('.fa') or reference.endswith('.mfa')
        self.samtools = samtools
        self.bowtie2 = bowtie2
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
        else:
            self.query = os.path.expanduser(self.query[0])
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')
        self.statistics = None
        self.version = None
        self.index = None
        self.verbosity = 1
        self.logger = logger
        self.readgroups = readgroups
        self.options = options
        self.unmapped = unmapped
        if self.unmapped is not None:
            if self.query_paired:
                if self.options is not None:
                    self.options += ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
                else:
                    self.options = ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
            else:
                if self.options is not None:
                    self.options += ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
                else:
                    self.options = ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
        if tmp is None:
            self.index_location = os.path.expanduser('~/tmp/{}'.format('.'.join(os.path.basename(reference).split('.')[:-1])))
        else:
            self.index_location = os.path.join(tmp, '.'.join(os.path.basename(reference).split('.')[:-1]))
        try:
            os.mkdir(os.path.expanduser('~/tmp/'))
        except OSError:
            pass
        self.report()
        #TODO options may change index_location or verbosity
    
    def update(self, query, output, options):
        self.query = query
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            if self.unmapped is not None:
                if options is not None:
                    self.options = options + ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
                else:
                    self.options = ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
        else:
            self.query = os.path.expanduser(self.query[0])
            if self.unmapped is not None:
                if options is not None:
                    self.options = options + ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
                else:
                    self.options = ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')

    def create_index(self):
        if self.options is not None and '-x' in self.options:
            self.index = None
        else:
            try:
                if self.verbosity >= 1:
                    log.print_write(self.logger, 'info', '=== Building index ===')
                try:
                    if self.reference == '':
                        raise Exception('No reference given.')
                except:
                    log.print_write(self.logger, 'exception', 'ERROR: No reference given.')
                    sys.exit(1)
                self.command = [x for x in [os.path.realpath(os.path.join(self.bowtie2, 'bowtie2-build')), '-f' if self.reference_fasta else None, self.reference, self.index_location] if x is not None]
                log.call(self.command, self.logger)
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.create_index()')
                return False
            self.index = self.index_location

    def map(self):
        if self.index is None:
            self.create_index()
        try:
            if self.readgroups:
                identifier = ''
                if (self.query[0] if self.query_paired else self.query).endswith('.gz'):
                    with gzip.open(self.query[0] if self.query_paired else self.query, 'rt') as read:
                        for line in read:
                            if line[0] == '@':
                                identifier = line[1:].split(' ')[0].strip()
                                break
                else:
                    with open(self.query[0] if self.query_paired else self.query, 'r') as read:
                        for line in read:
                            if line[0] == '@':
                                identifier = line[1:].split(' ')[0].strip()
                                break
                try:
                    match = re.match("([\w-]+):(\d+):([\w-]+):(\d+):(\d+):(\d+):(\d+)", identifier)
                    instrument = match.group(1)
                    run = match.group(2)
                    flowcell = match.group(3)
                    lane = match.group(4)
                    tile = match.group(5)
                    x_pos = match.group(6)
                    y_pos = match.group(7)
                except:
                    log.print_write(self.logger, 'warning', 'ERROR in setting readgroups. readgroups not set. Using default flowcell and lane.')
                    flowcell="unset"
                    lane="1000"

                if self.query_paired:
                    try:
                        rg_sm = re.match("r\d_[UP]+_(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(self.query[0])).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query[0]).split('.')[0]
                else:
                    try:
                        rg_sm = re.match("r\d_[UP]+_(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(self.query)).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query).split('.')[0]
                rg_pl = 'ILLUMINA'
                rg_id = '.'.join([flowcell, rg_sm, lane])
                rg_pu = rg_id
                rg_lb = 'lib1'
            else:
                rg_sm = None
                rg_pl = None
                rg_id = None
                rg_pu = None
                rg_lb = None

            if self.verbosity >= 1:
                log.print_write(self.logger, 'info', '=== Mapping query on reference ===')
            self.command = [x for x in ([
                    os.path.realpath(os.path.join(self.bowtie2, 'bowtie2')),
                    '-f' if self.query_fasta else None,
                    '-x' if self.index else None,
                    self.index if self.index else None,
                    '-1' if self.query_paired else None,
                    self.query[0] if self.query_paired else None,
                    '-2' if self.query_paired else None,
                    self.query[1] if self.query_paired else None,
                    '-U' if not self.query_paired else None, 
                    self.query if not self.query_paired else None,
                    '-S', self.output,
                    '--rg-id' if self.readgroups else None, rg_id,
                    '--rg' if self.readgroups else None, 'SM:{}'.format(rg_sm) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'PU:{}'.format(rg_pu) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'PL:{}'.format(rg_pl) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'LB:{}'.format(rg_lb) if self.readgroups else None] 
                    + (self.options.split(' ') if self.options else list())
                    )
                if x is not None] # TODO options
            self.statistics = log.check_output(self.command, self.logger, stderr=sp.STDOUT)
            return self.output
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.map()')
            return None

    def set_version(self):
        try:
            self.command = [os.path.realpath(os.path.join(self.bowtie2, 'bowtie2')), '--version']
            self.version = re.match('.*version (.*)\n', str(sp.check_output(self.command), 'utf-8')).group(1)
        except sp.CalledProcessError:
            log.write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.set_version')
            return False

    def get_version(self):
        if self.version is None:
            self.set_version()
        return self.version

    def get_statistics(self):
        return self.statistics 

    def get_fast_options(self):
        return '-k 10'

    def get_sensitive_options(self):
        return '-a'

    def cleanup(self):
        if self.index is not None:
            if self.verbosity >= 1:
                log.write(self.logger, 'info', '=== Cleaning up mapper ===')
            try:
                self.command = ('find {} -type f -regextype posix-extended '
                        '-iregex \'.*{}\.(([0-9]+\.bt2)|(rev\.[0-9]+\.bt2)|fasta)$\' -delete'.format(os.path.dirname(self.index), os.path.basename(self.index)))
                log.call(self.command, self.logger, shell=True)
                self.index = None
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.cleanup()')
                return False

    def report(self):
        log.write(self.logger, 'warning', 'Bowtie2 Version: {}'.format(self.get_version()))
        log.write(self.logger, 'warning', 'Parameters: ')
        for arg in sorted(vars(self)):
            if arg != 'command':
                log.write(self.logger, 'warning', '{} = {}'.format(arg, getattr(self, arg)))

class BWAMem:

    def __init__(self, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bwa=None):
        self.query = query
        self.reference = os.path.expanduser(reference)
        self.query_paired = len(query) == 2 #isinstance(query, list)
        self.output = os.path.expanduser('{}.sam'.format(output))
        self.samtools = samtools
        self.bwa = bwa
        self.unmapped_raw = unmapped
        self.unmapped = ''
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query[0]))[0])
        else:
            self.query = os.path.expanduser(self.query[0])
            self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query))[0])
        if self.unmapped_raw is None:
            self.unmapped = None
        self.statistics = None
        self.version = None
        self.index = None
        self.options = options
        self.index_given = '-x' in self.options if self.options is not None else False
        self.logger = logger
        self.verbosity = 1
        self.readgroups = readgroups
        if tmp is None:
            self.index_location = os.path.expanduser('~/tmp/{}'.format('.'.join(os.path.basename(reference).split('.')[:-1])))
        else:
            self.index_location = os.path.join(tmp, '.'.join(os.path.basename(reference).split('.')[:-1]))
        self.report()

    def update(self, query, output, options):
        self.query = query
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            if self.unmapped_raw is not None:
                self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query[0]))[0])
        else:
            self.query = os.path.expanduser(self.query[0])
            if self.unmapped_raw is not None:
                self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query))[0])
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')
    
    def create_index(self):
        if self.options is not None and '-x' in self.options:
            cache = self.options.split(' ')
            pos = cache.index('-x')
            self.index_location = cache[pos + 1]
            del cache[pos:pos+2]
            self.options = ' '.join(cache)
        else:
            try:
                if self.verbosity >= 1:
                    log.print_write(self.logger, 'info', '=== Building index ===')
                try:
                    if self.reference == '':
                        raise Exception('No reference given.')
                except:
                    log.print_write(self.logger, 'exception', 'ERROR: No reference given.')
                    sys.exit(1)
                self.command = [os.path.realpath(os.path.join(self.bwa, 'bwa')), 'index', '-p', self.index_location, self.reference]
                log.call(self.command, self.logger)
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.create_index()')
                return False
        self.index = self.index_location

    def map(self):
        if self.index is None:
            self.create_index()
        try:
            if self.readgroups:
                identifier = ''
                if (self.query[0] if self.query_paired else self.query).endswith('.gz'):
                    with gzip.open(self.query[0] if self.query_paired else self.query, 'rt') as read:
                        for line in read:
                            if line[0] == '@':
                                identifier = line[1:].split(' ')[0].strip()
                                break
                else:
                    with open(self.query[0] if self.query_paired else self.query, 'r') as read:
                        for line in read:
                            if line[0] == '@':
                                identifier = line[1:].split(' ')[0].strip()
                                break
                try:
                    match = re.match("([\w-]+):(\d+):([\w-]+):(\d+):(\d+):(\d+):(\d+)", identifier)
                    instrument = match.group(1)
                    run = match.group(2)
                    flowcell = match.group(3)
                    lane = match.group(4)
                    tile = match.group(5)
                    x_pos = match.group(6)
                    y_pos = match.group(7)
                except:
                    log.print_write(self.logger, 'warning', 'ERROR in setting readgroups. readgroups not set. Using default flowcell and lane.')
                    flowcell="unset"
                    lane="1000"

                if self.query_paired:
                    try:
                        rg_sm = re.match("r\d_[UP]+_(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(self.query[0])).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query[0]).split('.')[0]
                else:
                    try:
                        rg_sm = re.match("r\d_[UP]+_(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(self.query)).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query).split('.')[0]
                rg_pl = 'ILLUMINA'
                rg_id = '.'.join([flowcell, rg_sm, lane])
                rg_pu = rg_id
                rg_lb = 'lib1'
            else:
                rg_sm = None
                rg_pl = None
                rg_id = None
                rg_pu = None
                rg_lb = None

            if self.verbosity >= 1:
                log.print_write(self.logger, 'info', '=== Mapping query on reference ===')
            self.command = ' '.join([x for x in [
                os.path.realpath(os.path.join(self.bwa, 'bwa')),
                'mem',
                self.index,
                self.query[0] if self.query_paired else None,
                self.query[1] if self.query_paired else None,
                self.query if not self.query_paired else None,
                '-R' if self.readgroups else None, '\"@RG\\tID:{}\\tPU:{}\\tSM:{}\\tPL:{}\\tLB:{}\"'.format(rg_id, rg_pu, rg_sm, rg_pl, rg_lb) if self.readgroups else None,
                self.options if self.options else None,
                '>', self.output
                ] if x is not None]) # TODO options
            self.statistics = log.check_output(self.command, self.logger, shell=True, stderr=sp.STDOUT)
            if self.unmapped is not None:
                self.command = '{samtools} view -f4 {output} | {samtools} view -Sb - | {samtools} fastq - -1 {unmapped}.1.fastq -2 {unmapped}.2.fastq -s {unmapped}.singleton.fastq -0 {unmapped}.unpaired.fastq'.format(output=self.output, unmapped=self.unmapped, samtools=os.path.realpath(os.path.join(self.samtools, 'samtools')))
                log.call(self.command, self.logger, shell=True)
                try:
                    if os.path.getsize('{}.1.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.1.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.2.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.2.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.singleton.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.singleton.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.unpaired.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.unpaired.fastq'.format(self.unmapped))
                except:
                    pass
            return self.output
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.map()')
            return None

    def set_version(self):
        try:
            self.command = [os.path.realpath(os.path.join(self.bwa, 'bwa'))]
            p = re.compile('.*Version: (.*?)\n', re.DOTALL)
            self.version = re.match(p, str(sp.Popen(self.command,stdout=sp.PIPE,stderr=sp.STDOUT).communicate()[0], 'utf-8')).group(1)
        except sp.CalledProcessError as e:
            log.write(self.logger, 'exception', 'ERROR: CPE in BWAMem.set_version')
            return False

    def get_version(self):
        if self.version is None:
            self.set_version()
        return self.version

    def get_statistics(self):
        return self.statistics 
    
    def cleanup(self):
        if self.index is not None and not self.index_given:
            if self.verbosity >= 1:
                log.write(self.logger, 'info', '=== Cleaning up mapper===')
            try:
                self.command = ('find {} -type f -regextype posix-extended '
                        '-iregex \'.*{}\.(amb|ann|bwt|pac|sa|fasta)$\' -delete'.format(os.path.dirname(self.index), os.path.basename(self.index)))
                log.call(self.command, self.logger, shell=True)
                self.index = None
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.cleanup()')
                return False
    
    def report(self):
        log.write(self.logger, 'warning', 'BWA Version: {}'.format(self.get_version()))
        log.write(self.logger, 'warning', 'Parameters: ')
        for arg in sorted(vars(self)):
            if arg != 'command':
                log.write(self.logger, 'warning', '{} = {}'.format(arg, getattr(self, arg)))

# add new mappers here
def list_mapper():
    return ('bowtie2', 'bwa-mem')

# add new mappers here
def get_mapper(mapper_key, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bwa=None, bowtie2=None): 
    if mapper_key == 'bowtie2':
        return Bowtie2(query, reference, output, tmp=tmp, options=options, logger=logger, readgroups=readgroups, unmapped=unmapped, samtools=samtools, bowtie2=bowtie2)
    if mapper_key == 'bwa-mem':
        return BWAMem(query, reference, output, tmp=tmp, options=options, logger=logger, readgroups=readgroups, unmapped=unmapped, samtools=samtools, bwa=bwa)
