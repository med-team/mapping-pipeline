#!/usr/bin/env cwl-runner
class: CommandLineTool
inputs:
  - id: "#mapper"
    type: Any
    inputBinding:
      position: 1
  - id: "#read1"
    type: Any
    inputBinding:
      position: 2
  - id: "#reference"
    type:
      type: array
      items: string
      inputBinding:
        prefix: --reference
        separate: true
  - id: "#read2"
    type: ["null",Any]
    inputBinding:
      prefix: --read2
  - id: "#noreport"
    type: ["null",boolean]
    default: false
    inputBinding:
      prefix: --no-report
  - id: "#output"
    type: Any
    inputBinding:
      prefix: --output
  - id: "#options"
    type: ["null",string]
    inputBinding:
      prefix: --options
  - id: "#reffinder_options"
    type: ["null",Any]
    inputBinding:
      prefix: --reffinder_options
outputs: []
baseCommand: ["./mapping.py"]
