# Mapping Pipeline

## Dependencies

This tool was implemented in Python and needs Python 3.4 or higher.

Required software:
* [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* [BWA](http://bio-bwa.sourceforge.net/)
* [Latex*](https://www.latex-project.org/)
* [samtools](http://www.htslib.org/)
* [Picard*](https://broadinstitute.github.io/picard/)

At least one mapping software is needed (BWA or Bowtie2). <br>
*To generate an output report Latex is needed. <br>
*To merge all SAM files in a project Picard is needed.

Required Python packages:
* jinja2
* matplotlib
* scipy
* numpy
* pylab
* pysam

---

## Usage

```
./mapping.py <mapper> <read1> <option(s)>
```

required input:
* mapper: specify mapper to use {bowtie2, bwa-mem}
* read1: either unpaired reads, first mate of paired reads, project folder or folder containing project folders. <br>
&nbsp;&nbsp;&nbsp;&nbsp;When given a folder, the read names must contain a certain prefix to be recognized. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For paired reads: "r1\_P\_" and "r2\_P\_" for first and second mates, respectively. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For unpaired reads: "r1\_UP\_" or "r2\_UP\_".

options:
* --read2 \<read2\>: second mate of paired reads
* --no-report: do not generate report
* --output \<out\>: set output directory
* --options "\<options\>": set options for mapper
* --reffinder_options="\<options\>": set options for reffinder
* --reference \<reference\>: location of the reference genome in either FASTA or GenBank format
* --join: join all SAM files within project, needs --picard
* --join_sample: join all SAM files per sample, needs --picard
* --multi-fasta: Splits multifasta files into single fasta files to use with reffinder.
* --no-rg: Do no set read groups.
* --unmapped \<path/to/folder/unmapped\>: Write unmapped reads to file. '.fastq' is automatically added.
* --samtools \<samtools_folder\>: Location of folder containing samtools.
* --picard \<picard_folder\>: Location of folder containing picard.jar.
* --bwa \<bwa_folder\>: Location of folder containing bwa.
* --java \<java\>: Java 1.8+ executable
* --bowtie2 \<bowtie2_folder\>: Location of folder containing bowtie2.
* --config <path/to/file>: Load parameter from file.

config file:<br>
You can also use a config file containing parameters.<br>
example.config:
```
--samtools=/usr/bin/
--bwa=/usr/bin/
--bowtie2=/usr/bin/
```

Example usage:
```
./mapping.py <...> --config /home/example.config
```



###### GenBank file
GenBank files may be acquired from [NCBI](http://www.ncbi.nlm.nih.gov/genbank/).  
Make sure to include the sequence (*Customize view* -> *Display options* -> *Show sequence*) before downloading the file (*Send* -> *Complete record* -> *File* -> *GenBank (full)*).

---

## Included Files

This pipeline consists of ten files:
* mapping.py: main script for the Mapping Pipeline
* mapper.py: manages different mappers
* metrics.py: creates report
* custom_logger.py: contains functions for writing log files
* mapping.cwl: provides interface for use with cwl-runner
* report.tex: template for report
* extract_genbank.py: Originally written by Wojtek Dabrowski. Extracts sequence from Genbank file.
* reffinder.py: Written by Stephan Fuchs. Provides best reference for mapping when given multiple references.
* fux_filing.py: Written by Stephan Fuchs. Provides functions for reffinder.
* example.config: Example config file.

---

## Example

```
./mapping.py bwa-mem --reference path/reference1.fasta --reference path/reference2.gb path/read1.fastq --read2 path/read2.fastq --output path --options "-M"
./mapping.py bwa-mem path/read1.fastq --read2 path/read2.fastq --option "-x /index/bwa"
./mapping.py bowtie2 path/to/project --reference path/to/reference --join path/to/picard
```

---

## Report

The report includes general information about the run, e.g. start time, user and options.
Furthermore, several metrics are computed:
* total reads
* concordant reads
* discordant reads
* paired reads
* mapped reads
* unmapped reads
* coverage graph
* bases per coverage graph
* reads per mapping quality graph
* mapping quality graph
* average coverage
* maximum coverage
* minimum coverage
* standard deviation of coverage
* number of bases with coverage of 0
* number of bases with coverage > mean+2*sd
* number of bases with coverage < mean+2*sd