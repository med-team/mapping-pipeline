#!/usr/bin/env python3
import argparse
from collections import defaultdict
import configparser
import datetime
import imp
import jinja2
import logging
import os
import platform
import pwd
import re
import socket
import string
import sys
if sys.version_info < (3, 2):
    import subprocess32 as sp
else:
    import subprocess as sp
### 
import custom_logger as log
import datetime
import extract_genbank
import reffinder
import mapper
import metrics

__author__ = "Enrico Seiler"
__credits__ = ["Wojtek Dabrowski"]
__license__ = "GPL"
__version__ = "1.2.1"
__maintainer__ = "Enrico Seiler"
__email__ = "seilere@rki.de"
__status__ = "Development"

class MAPPING:
    def __init__(self, logger=None):
        self.name = 'Mapping Pipeline'
        self.version = __version__
        self.logger = logger
        self.python_version = sys.version.split(' ')[0]
        self.reffinder_version = reffinder.__version__
        self._argument_parser()
        self.args.output = os.path.realpath(self.args.output)
        self.user = pwd.getpwuid(os.getuid()).pw_name
        self.server = socket.getfqdn()
        self.system = platform.system()
        self.machine = platform.architecture()[0]
        self.call = ' '.join(sys.argv)
        if self.system == 'Linux':
            self.linux_dist = platform.linux_distribution()[0]      # Ubuntu
            self.linux_version = platform.linux_distribution()[1]   # 14.04
            self.release = ' '.join([self.linux_dist, self.linux_version])
        elif self.system == 'Windows':                      # Windows
            self.windows_version = platform.win32_ver()[0]  # 7
            self.windows_build = platform.win32_ver()[1]    # 6.1.7601
            self.windows_csd = platform.win32_ver()[2]      # SP1
            self.release = ' '.join([self.windows_version, self.windows_csd, self.windows_build])
        elif self.system == 'Java':
            # ...
            pass
        else:
            self.mac_release = platform.mac_ver()[0]
            self.mac_version = platform.mac_ver()[1][0]
        self.metrics_results = dict()
        self.mapper = None
        self.sam_files = list()
        self.delete = list()
        self.reads = list() # save read names for report
        self.read_dict = dict() # save reads per sample for report
        self.tmp = os.path.join(os.path.expanduser('~/tmp/'), datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f'))
        try:
            os.mkdir(os.path.expanduser('~/tmp/'))
        except OSError:
            pass
        try:
            os.mkdir(self.tmp)
        except OSError:
            pass
        self.samtools_version = self._samtools_version()
        self.picard_version = self._picard_version()
        self.run()
        if self.args.join:
            self._join_sample()
            self.run_metrics()
            self._join()
        elif self.args.join_sample:
            self._join_sample()
            self.run_metrics()
        else:
            self.run_metrics()
        self.convert_to_bam()
        self.cleanup()

    def _samtools_version(self):
        cmd = '{samtools}/samtools --help'.format(samtools=self.args.samtools)
        out = str(sp.check_output(cmd, shell=True), 'utf-8')
        pattern = re.compile('.*Version: ([\d\.]+) .*', re.DOTALL)
        return re.match(pattern, out).group(1)

    def _picard_version(self):
        try:
            cmd = '{java} -jar {picard}/picard.jar ViewSam --version'.format(java=self.args.java, picard=self.args.picard)
            out = ''
            try:
                out = str(sp.check_output(cmd, shell=True, stderr=sp.STDOUT), 'utf-8')
            except sp.CalledProcessError as e:
                out = str(e.output, 'utf-8')
            pattern = re.compile('([\d\.]+)\(.*', re.DOTALL)
            return re.match(pattern, out).group(1)
        except:
            return 'Not used'

    def run_metrics(self):
        if not self.args.noreport:
            for sam in self.sam_files:
                out = os.path.join(self.args.output, 'data', os.path.basename(sam))
                os.makedirs(out, exist_ok=True)
                if self.args.join_sample or self.args.join:
                    self.reads = self.read_dict[os.path.basename(sam)+'.sam']
                self.sam = sam
                self.metric = metrics.METRICS(sam + '.sam', output=out, logger=self.logger, samtools=os.path.realpath(os.path.join(self.args.samtools, 'samtools')))
                self.current_project = os.path.basename(self._sam_name(os.path.basename(sam))) #read1
                self.metrics_results[self.current_project] = dict()
                self.metric.read_stats()
                self.metric.compute_metrics()
                self.metrics_results[self.current_project] = self.metric.cov_stats.copy()
                self.metrics_results[self.current_project].update(self.metric.read_stats)
                self.end_time = self._timestr(d=datetime.datetime.now())
                self.report()
        
    def report(self):
        self.report_file = os.path.join(self.args.output, 'mapping_{}_{}.tex'.format(os.path.basename(self._sam_name(self.sam)), datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%y_%H-%M')))
        # self.env = jinja2.Environment()
        # self.env.loader = jinja2.FileSystemLoader('.')

        # self.template = self.env.get_template(os.path.join(os.path.dirname(__file__), 'report.tex'))
        # self.pdf_latex = self.template.render(pipeline=self, metric=self.metric)
        template_dir = os.path.dirname(__file__) #'/usr/share/mapping-pipeline'
        template_file = 'report.tex'
        loader = jinja2.FileSystemLoader(template_dir)
        env = jinja2.Environment(loader=loader)
        template = env.get_template(template_file)
        try:
            if type(self.args.reference) == tuple or type(self.args.reference) == list:
                self.args.reference = self.args.reference[0]
            elif self.args.reference is None or len(self.args.reference) == 0:
                self.args.reference = 'User defined index'
        except:
            self.args.reference = 'User defined index'
        pdf_latex = template.render(pipeline=self, metric=self.metric)

        with open(self.report_file, 'w') as self.latex:
            self.latex.write(pdf_latex)
        
        process = sp.Popen(['pdflatex', '-interaction=nonstopmode', '-output-directory={}'.format(os.path.dirname(self.report_file)), self.report_file], stdout = sp.PIPE, stderr = sp.PIPE)
        for line in iter(process.stderr.readline, b''):
            print(line)

        process.communicate()
        self.metric.cleanup()
    def cleanup(self):
        log.print_write(self.logger, 'info', '=== Cleaning up ===')
        try:
            self.command = ('find {} -type f -regextype posix-extended '
                    '-iregex \'.*\.(aux|log|tex)$\' -delete'.format(os.path.realpath(self.args.output)))
            log.call(self.command, self.logger, shell=True)
            self.mapper.cleanup()
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in MAPPING.cleanup()')
            return False
        try:
            self.command = 'rm -dfr {}'.format(self.tmp)
            log.call(self.command, self.logger, shell=True)
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in MAPPING.cleanup()')
            pass
        try:
            for delete in self.delete:
                self.command = 'rm -f {}'.format(delete)
                log.call(self.command, self.logger, shell=True)
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in MAPPING.cleanup()')
            pass
    def convert_to_bam(self):
        log.print_write(self.logger, 'info', '=== Converting to BAM file ===')
        for sam in self.sam_files:
            sam = sam + '.sam'
            self.command = '{} view -hb {} -o {}'.format(os.path.realpath(os.path.join(self.args.samtools, 'samtools')), sam, sam[:-3] + 'bam')
            log.call(self.command, self.logger, shell=True)
            self.command = 'rm {}'.format(sam)
            log.call(self.command, self.logger, shell=True)
    def run_reffinder(self, mode, walk=None):
        log.print_write(self.logger, 'info', '=== Running Reffinder ===')
        if self.args.reffinder_options is None:
            if mode == 0:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} {} --ref {} --tmp {}".format(self.args.read1, self.args.read2, ' '.join(self.args.reference), self.tmp), logger=self.logger))
            elif mode == 1:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} --ref {} --mode single --tmp {}".format(self.args.read1, ' '.join(self.args.reference), self.tmp), logger=self.logger))
            elif mode == 2:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} {} --ref {} --tmp {}".format(os.path.join(walk, self.r), os.path.join(walk, self.r2), ' '.join(self.args.reference), self.tmp), logger=self.logger))
            else:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} --ref {} --mode single --tmp {}".format(os.path.join(walk, self.r), ' '.join(self.args.reference), self.tmp), logger=self.logger))
        else:
            if mode == 0:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} {} --ref {} {} --tmp {}".format(self.args.read1, self.args.read2, ' '.join(self.args.reference), self.args.reffinder_options, self.tmp), logger=self.logger))
            elif mode == 1:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} --ref {} --mode single {} --tmp {}".format(self.args.read1, ' '.join(self.args.reference), self.args.reffinder_options, self.tmp), logger=self.logger))
            elif mode == 2:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} {} --ref {} {} --tmp {}".format(os.path.join(walk, self.r), os.path.join(walk, self.r2), ' '.join(self.args.reference), self.args.reffinder_options, self.tmp), logger=self.logger))
            else:
                self.args.reference = reffinder.find_reference(reffinder.parse_arguments("--fastq {} --ref {} --mode single {} --tmp {}".format(os.path.join(walk, self.r), ' '.join(self.args.reference), self.args.reffinder_options, self.tmp), logger=self.logger))
        if type(self.args.reference) == tuple:
            self.args.reference = self.args.reference[0]
    def convert_to_accession(self):
        pass
        #pattern = re.compile('>gi\|\d*\|[^|]*\|([^|]*)\|(.*)')
        #with open(self.args.reference[0], 'r+') as f:
        #    d = f.readlines()
        #    f.seek(0)
        #    fl = d[0]
        #    del(d[0])
        #    if '>gi' in fl:
        #        f.write('>{}\n'.format(' '.join(re.match(pattern, fl).groups())))
        #        for l in d:
        #            f.write(l)
        #        f.write('\n')
        #        f.truncate()
    def run(self):
        if self.args.reference is not None:
            # Handle genbank
            new_ref = []
            for ref in self.args.reference:
                if ref.endswith('.gb') or ref.endswith('.gbk'):
                    new_ref.append(extract_genbank.main(ref, os.path.join(self.tmp, os.path.basename(''.join(ref.split('.')[:-1])+'.fasta'))))
                else:
                    new_ref.append(ref)
            self.args.reference = new_ref
            # if --multi-fasta is set, split refs
            if self.args.multifasta:
                print('=== Splitting multifasta ===')
                self.split_multifasta()
            #TODO RefFinder, if set to run, change self.args.reference to the returned reference.
            self.find_ref = len(self.args.reference) >= 2
            # Convert gi to accession
            self.convert_to_accession()
        else:
            self.find_ref = False
        # If only reads were given, run the pipeline on the reads
        if not os.path.isdir(self.args.read1):
            self.read_counter = 1
            if self.find_ref:
                if self.args.read2 is not None:
                    self.run_reffinder(0)
                else:
                    self.run_reffinder(1)
            elif self.args.reference:
                self.args.reference = self.args.reference[0]
            self._run_once(self.args.read1, self.args.read2)
        # Else we have a folder containing project folders with reads. Run the pipeline for each project.
        else:
            self.read_counter = 1
            [self.root, self.subdir, self.files] = next(os.walk(self.args.read1))
            for self.sub in self.subdir:
                self._run_all(next(os.walk(os.path.join(self.root, self.sub))))
            self._run_all([self.root, self.subdir, self.files])
    def _run_all(self, walk):
        # Run pipeline for each pair of reads.
        for self.r in [x for x in walk[2] if x.endswith('.fastq') or x.endswith('fastq.gz')]:
            if self.r.startswith('r1_P'):
                self.r2 = self.r[0] + '2' + self.r[2:]#TODO regex?
                if not os.path.isfile(self.r2):
                    try:
                        # QCumber output
                        pattern = '(r)([1,2])(_P_.*_L\d\d\d_)(R)([1,2])(_\d\d\d\.fastq.*)'
                        frags = re.match(pattern, self.r).groups()
                        self.r2 = '{}{}{}{}{}{}'.format(frags[0], '2', frags[2], frags[3], '2', frags[5])
                    except AttributeError:
                        # Generic
                        pattern = '(r)([1,2])(_P_.*)'
                        frags = re.match(pattern, self.r).groups()
                        self.r2 = '{}{}{}{}{}{}'.format(frags[0], '2', frags[3])
                if self.args.reference is not None:
                    self.cache_ref = self.args.reference
                    if self.find_ref:
                        self.run_reffinder(2, walk[0])
                    else:
                        self.args.reference = self.args.reference[0]
                self.reads.append(self.r)
                self.reads.append(self.r2)
                print('RUNNING {} and {}'.format(self.r, self.r2))
                self._run_once(os.path.join(walk[0], self.r), read2=os.path.join(walk[0], self.r2))
                if self.args.reference is not None:
                    self.args.reference = self.cache_ref
                self.read_counter += 1
            elif self.r.startswith('r2_P'):
                pass
            else:
                if self.args.reference is not None:
                    self.cache_ref = self.args.reference
                    if self.find_ref:
                        self.run_reffinder(3, walk[0])
                    else:
                        self.args.reference = self.args.reference[0]
                self.reads.append(self.r)
                print('RUNNING {}'.format(self.r))
                self._run_once(os.path.join(walk[0], self.r))
                if self.args.reference is not None:
                    self.args.reference = self.cache_ref
                self.read_counter += 1
    def _sam_name(self, s):
        if s.startswith('r1_P_') or s.startswith('r2_P_'):
            st = 'P_' + s[5:]
        elif s.startswith('r1_UP_') or s.startswith('r2_UP_'):
            st = 'UP_' + s[6:]
        else:
            st = s
        try:
            self.sam_name = re.match('(.*)(\.fastq\.gz|\.fastq)$', st).group(1)
        except:
            st = os.path.basename(st)
            self.sam_name =  '.'.join(st.split('.')[:-1])
        if self.sam_name == '':
            self.sam_name = os.path.basename(s)
        return os.path.join(self.args.output, self.sam_name)
    def _join(self):
        log.print_write(self.logger, 'info', '=== Joining SAM files ===')
        command = '{java} -jar {jar} MergeSamFiles O={o} '.format(java=self.args.java, jar=os.path.join(self.args.picard, 'picard.jar'), o=os.path.join(self.args.output, 'joined.sam'))
        for sam in self.sam_files:
            command += 'I={}.sam '.format(sam)
            self.delete.append(sam + '.sam')
        log.call(command, self.logger, shell=True)
        self.sam_files = [os.path.join(self.args.output, 'joined')]
    def _join_sample(self):
        log.print_write(self.logger, 'info', '=== Joining SAM files per sample ===')
        groups = defaultdict(set)
        pattern = re.compile('^U{0,1}P_(.*)_L\d\d\d_R[1,2]_\d\d\d')
        try:
            for sam in self.sam_files:
                sam = os.path.basename(sam)
                sample = re.match(pattern, sam).group(1)
                groups[sample].add(sam)
        except:
            pattern = re.compile('^U{0,1}P_(.*)')
            for sam in self.sam_files:
                sam = os.path.basename(sam)
                sample = re.match(pattern, sam).group(1)
                groups[sample].add(sam)
        self.sam_files = list()
        for (key, values) in groups.items():
            command = '{java} -jar {jar} MergeSamFiles O={o} '.format(java=self.args.java, jar=os.path.join(self.args.picard, 'picard.jar'), o=os.path.join(self.args.output, key+'.sam'))
            for sam in values:
                command += 'I={}.sam '.format(os.path.join(self.args.output, sam))
                self.delete.append(os.path.join(self.args.output, sam + '.sam'))
            log.call(command, self.logger, shell=True)
            self.sam_files.append(os.path.join(self.args.output, key))
            self.read_dict[key+'.sam'] = [i for i in self.reads if key in i]
    def _run_once(self, read1, read2=None):
        log.print_write(self.logger, 'info', '=== Processing read #{} ==='.format(self.read_counter))
        log.write(self.logger, 'info', 'Read1 = {}'.format(read1))
        log.write(self.logger, 'info', 'Read2 = {}'.format(read2))
        self.start_time = self._timestr(d=datetime.datetime.now())
        self.current_read1 = read1
        self.current_read2 = read2
        # Mapping
        self.sam_files.append(self._sam_name(os.path.basename(read1)))
        if self.mapper is None:
            self.mapper = mapper.get_mapper(self.args.mapper, [x for x in [read1, read2] if x is not None], self.args.reference if self.args.reference else '', self._sam_name(os.path.basename(read1)), self.tmp, self.args.options, self.logger, not self.args.norg, os.path.realpath(self.args.unmapped) if self.args.unmapped is not None else None, self.args.samtools, self.args.bwa, self.args.bowtie2)
        else:
            self.mapper.update([x for x in [read1, read2] if x is not None], self._sam_name(os.path.basename(read1)), self.args.options)
        self.sam_file = self.mapper.map()
        self.mapper_version = self.args.mapper + ' ' + self.mapper.get_version()
        self.sam_files.append(self._sam_name(self.sam_file))
    def split_multifasta(self):
        f = None
        files = list()
        for ref in self.args.reference:
            for line in open(ref, 'r'):
                if '>' in line:
                    if f is not None:
                        f.close()
                    name = os.path.join(self.tmp, '{}.fasta'.format(line.split(' ')[0][1:].strip())) if len(line.split(' ')) >= 2 else os.path.join(self.tmp, '{}.fasta'.format(line[1:].strip()))
                    f = open(name, 'w')
                    files.append(name)
                    f.write('{}'.format(line))
                else:
                    f.write('{}'.format(line))
        if f is not None:
            f.close
        self.args.reference = files

    def _valid_picard(self, f):
        if os.path.isfile(os.path.join(os.path.realpath(f), 'picard.jar')):
            return f
        else:
            raise argparse.ArgumentTypeError('Cannot find picard.jar in folder {}'.format(os.path.realpath(f)))

    def _valid_unmapped(self, f):
        if not os.path.isdir(os.path.realpath(f)):
            return f
        else:
            raise argparse.ArgumentTypeError('{} is a directory. Please specify a path containing the prefix for unmapped reads.'.format(os.path.realpath(f)))

    def _valid_samtools(self, f):
        if os.path.isfile(os.path.join(os.path.realpath(f), 'samtools')):
            return f
        else:
            raise argparse.ArgumentTypeError('Cannot find samtools in folder {}'.format(os.path.realpath(f)))

    def _valid_java(self, f):
        try:
            version_string = sp.check_output(f + " -version", shell=True, stderr=sp.STDOUT)
        except:
            raise argparse.ArgumentTypeError('It appears that {} is not a java executable (absolute or available in the PATH environment)'.format(f))
        try:
            version = version_string.decode("ASCII").split("\n")[0].split('"')[1].split("_")[0]
            vn = float(".".join(version.split(".")[:2]))
        except:
            raise argparse.ArgumentTypeError('It appears that {} is not a java executable (absolute or available in the PATH environment)'.format(f))
        if vn < 1.8:
            raise argparse.ArgumentTypeError('{} is java version {}, however, at least version 1.8 is required'.format(f, vn))
        return f

    def _valid_bwa(self, f):
        if os.path.isfile(os.path.join(os.path.realpath(f), 'bwa')):
            return f
        else:
            raise argparse.ArgumentTypeError('Cannot find bwa in folder {}'.format(os.path.realpath(f)))

    def _valid_bowtie2(self, f):
        if os.path.isfile(os.path.join(os.path.realpath(f), 'bowtie2')):
            return f
        else:
            raise argparse.ArgumentTypeError('Cannot find bowtie2 in folder {}'.format(os.path.realpath(f)))

    def _argument_parser(self):
        # Parse any conf_file specification
        # We make this parser with add_help=False so that
        # it doesn't parse -h and print help.
        conf_parser = argparse.ArgumentParser(
            description=__doc__, # printed with -h/--help
            # Don't mess with format of description
            formatter_class=argparse.RawDescriptionHelpFormatter,
            # Turn off help, so we print all options in response to -h
            add_help=False
        )
        config = os.path.join(os.path.dirname(__file__), 'run.config')
        if '--config' not in sys.argv and os.path.isfile(config):
            sys.argv += ['--config', config]
        conf_parser.add_argument("--config", help="Specify config file", metavar="FILE")
        conf_args, remaining_argv = conf_parser.parse_known_args()
        if conf_args.config:
            with open(conf_args.config, 'r') as f:
                for line in f:
                    if '=' in line:
                        [option, argument] = line.strip().split('=')
                        if option not in sys.argv:
                            remaining_argv += [option, argument]
                    else:
                        if line.strip() not in sys.argv:
                            remaining_argv += [line.strip()]

        self.parser = argparse.ArgumentParser(description='Mapping pipeline', add_help=False)
        self.required = self.parser.add_argument_group('required arguments')
        self.optional = self.parser.add_argument_group('optional arguments')
        self.required.add_argument('mapper', type=self._is_mapper, help='Select a mapper. Supported: {}'.format(' '.join(mapper.list_mapper())))
        self.required.add_argument('read1', type=str, help='Either unpaired reads, first mate of paired reads, project folder or folder containing project folders.')
        self.optional.add_argument('--reference', dest='reference', type=str, help='Reference genome. FASTA or GenBank format. Parameter can be used multiple times.', action='append')
        self.optional.add_argument('-h', '--help', action='help', help='Show this help message and exit')
        self.optional.add_argument('--no-report', dest='noreport', action='store_true', default=False, help='Do not generate report.')
        self.optional.add_argument('--read2', dest='read2', nargs='?', type=str, help='Second mate of paired reads.', default=None)
        self.optional.add_argument('--output', dest='output', nargs='?', type=str, help='Output location. (Default: %(default)s)', default=os.getcwd())
        self.optional.add_argument('--multi-fasta', dest='multifasta', action='store_true', default=False, help='If set, split multifasta files into single fasta files.')
        self.optional.add_argument('--options', dest='options', nargs='?', type=str, help='Options for the mapper. Usage: --options "option(s)"')
        self.optional.add_argument('--reffinder_options', dest='reffinder_options', nargs='?', type=str, help='Options for reffinder. Usage: --reffinder_options="option(s)"')
        self.optional.add_argument('--join', dest='join', action='store_true', default=False, help='Join all created alignment files into one file.')
        self.optional.add_argument('--join_sample', dest='join_sample', action='store_true', default=False, help='Join all created alignment files per sample into one file.')
        self.optional.add_argument('--no-rg', dest='norg', action='store_true', default=False, help='Do not set read groups.')
        self.optional.add_argument('--unmapped', dest='unmapped', nargs='?', default=None, type=self._valid_unmapped, help='Write unmapped reads to given file. Read name will be automatically added.')
        self.optional.add_argument('--java', dest='java', nargs='?', default='java', type=self._valid_java, help='Java version 1.8+ executable.')
        self.optional.add_argument('--samtools', dest='samtools', nargs='?', default='/usr/bin', type=self._valid_samtools, help='Path to folder containing samtools.')
        self.optional.add_argument('--bwa', dest='bwa', nargs='?', default='/usr/bin', type=self._valid_bwa, help='Path to folder containg bwa.')
        self.optional.add_argument('--bowtie2', dest='bowtie2', nargs='?', default='/usr/bin/', type=self._valid_bowtie2, help='Path to folder containing bowtie2.')
        self.optional.add_argument('--picard', dest='picard', nargs='?', default='/usr/bin/', type=str, help='Path to folder containing picard.jar')
        self.optional.add_argument('--config', help='Define config file to read parameters from. If run.config exists in the source directory, it is if --config is not defined.')
        self.args = self.parser.parse_args(remaining_argv)
        if self.args.join_sample and self.args.join:
            raise argparse.ArgumentTypeError('Only either --join or --join_sample are allowed')
        if self.args.join_sample or self.args.join:
            self.args.picard = self._valid_picard(os.path.expanduser(self.args.picard))
        if self.args.options is not None:
            if self.args.options[0] == self.args.options[-1] == '"':
                self.args.options = self.args.options[1:-1] # workaround for cwl
    def _is_mapper(self, m):
        self.mapper_list = mapper.list_mapper()
        if m in self.mapper_list:
            return m
        else:
            raise argparse.ArgumentTypeError('Mapper {} is not supported. Available mappers are: {}'.format(m, ' '.join(self.mapper_list)))
    def _timestr(self,ger=False,d=None):
        def lz(i):
            if i < 10:
                return '0{}'.format(i)
            else:
                return i

        weekdays = {
                1: 'Monday',
                2: 'Tuesday',
                3: 'Wednesday',
                4: 'Thursday',
                5: 'Friday',
                6: 'Saturday',
                7: 'Sunday',
                8: 'Montag',
                9: 'Dienstag',
                10: 'Mittwoch',
                11: 'Donnerstag',
                12: 'Freitag',
                13: 'Samstag',
                14: 'Sonntag'
        }
        tstr = '{} {}.{}.{} {}:{}:{}'
        if d is None:
            d = datetime.datetime.now()
        if not ger:
            tstr = tstr.format(weekdays[d.isoweekday()], lz(d.day), lz(d.month), d.year, lz(d.hour), lz(d.minute), lz(d.second))
        else:
            tstr = tstr.format(weekdays[d.isoweekday()+7], lz(d.day), lz(d.month), d.year, lz(d.hour), lz(d.minute), lz(d.second))
        return tstr

def timestr(ger=False,d=None):
    def lz(i):
        if i < 10:
            return '0{}'.format(i)
        else:
            return i

    weekdays = {
            1: 'Monday',
            2: 'Tuesday',
            3: 'Wednesday',
            4: 'Thursday',
            5: 'Friday',
            6: 'Saturday',
            7: 'Sunday',
            8: 'Montag',
            9: 'Dienstag',
            10: 'Mittwoch',
            11: 'Donnerstag',
            12: 'Freitag',
            13: 'Samstag',
            14: 'Sonntag'
    }
    tstr = '{} {}.{}.{} {}:{}:{}'
    if d is None:
        d = datetime.datetime.now()
    if not ger:
        tstr = tstr.format(weekdays[d.isoweekday()], lz(d.day), lz(d.month), d.year, lz(d.hour), lz(d.minute), lz(d.second))
    else:
        tstr = tstr.format(weekdays[d.isoweekday()+7], lz(d.day), lz(d.month), d.year, lz(d.hour), lz(d.minute), lz(d.second))
    return tstr

def valid_filename(s):
    valid_chars = "-_.: %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    return filename.replace(' ','_').replace('.','-').replace(':','-')

if  __name__ == '__main__':
    if '-h' in sys.argv or '--help' in sys.argv:
        m = MAPPING(None)
    else:
        out = None
        try:
            out = sys.argv[sys.argv.index('--output')+1]
        except Exception:
            pass
        if out is None:
            try:
                cache = [y for x in sys.argv for y in x.split('=')]
                out = cache[cache.index('--output')+1]
            except Exception:
                pass
        if out is None:
            out = ''
        t0 = datetime.datetime.now()
        log_file = os.path.join(out, ('{}_log.txt'.format(valid_filename(timestr(d=t0)))))
        try:
            os.mkdir(os.path.dirname(log_file))
        except:
            pass
        logging.basicConfig(format='%(levelname)s:%(message)s',filename=log_file,filemode='w',level=logging.DEBUG)
        logger = logging.getLogger(log_file)
        logger.info('Log is written to: {}'.format(os.path.abspath(log_file)))
        logger.info('This pipeline was invoked by: {}'.format(pwd.getpwuid(os.getuid()).pw_name))
        logger.info('This pipeline started at: {}'.format(timestr(d=t0)))
        m = MAPPING(logger)
        t1 = datetime.datetime.now()
        logger.info('This pipeline finished at: {}'.format(timestr(d=t1)))
        logger.info('Runtime: {}'.format(t1-t0))


